package ru.spb.beavers.core.gui;

import ru.spb.beavers.core.components.FixedJPanel;
import ru.spb.beavers.core.data.StorageModules;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * View описания теоретического решения
 */
public class TheoryView extends JPanel {

    private final JPanel theoryScrollPanel = new FixedJPanel();
    private final JPanel theoryPanel = new JPanel();
    private final JButton btnGoDescription = new JButton("Описание");
    private final JButton btnGoInputs = new JButton("Решение");

    private final TheoryViewPresenter presenter = new TheoryViewPresenter();

    public TheoryView() {
        super(null);

        theoryScrollPanel.setBorder(BorderFactory.createLineBorder(Color.black));
        theoryScrollPanel.setBounds(20, 20, 850, 400);
        theoryScrollPanel.setLayout(new GridLayout(1, 1));
        this.add(theoryScrollPanel);

        JScrollPane theoryPanelWrapper = new JScrollPane(theoryPanel);
        theoryScrollPanel.add(theoryPanelWrapper);

        btnGoDescription.setSize(100, 30);
        btnGoDescription.setLocation(20, 450);
        this.add(btnGoDescription);

        btnGoInputs.setSize(100, 30);
        btnGoInputs.setLocation(770, 450);
        this.add(btnGoInputs);

        initListeners();
    }

    private void initListeners() {
        btnGoDescription.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoDescriptionPressed();
            }
        });

        btnGoInputs.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                presenter.btnGoInputsPressed();
            }
        });
    }

    public JPanel getTheoryPanel() {
        return theoryPanel;
    }

    @Override
    public void removeAll() {
        theoryPanel.removeAll();
    }

    private class TheoryViewPresenter {

        public void btnGoDescriptionPressed() {
            GUIManager.setActiveView(GUIManager.getDescriptionView());
        }

        public void btnGoInputsPressed() {
            InputView inputView = GUIManager.getInputView();
            StorageModules.getActiveModule().initInputPanel(inputView.inputPanel);
            GUIManager.setActiveView(inputView);
        }
    }
}
